import { findIndex } from "lodash";

export function getPlayerIdx(players, id) {
  return findIndex(players, (p) => Number(p.id) === Number(id));
}
