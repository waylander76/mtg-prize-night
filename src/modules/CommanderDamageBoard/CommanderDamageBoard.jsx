import React, { useCallback } from "react";
import { map } from "lodash";
import { Row, CounterController } from "components";
import { getPlayerIdx } from "utils";

import "./CommanderDamageBoard.css";

export const CommanderDamageBoard = ({
  opponents,
  addCommanderDmg,
  player,
}) => {
  const handleAddCommanderDmg = useCallback(
    (v, k) => {
      addCommanderDmg(v, k, player.id);
    },
    [addCommanderDmg, player.id]
  );

  return (
    <Row>
      {map(opponents, (o) => {
        return (
          <React.Fragment key={o.id}>
            <CounterController
              onCounterClick={handleAddCommanderDmg}
              counterVal={1}
              desc={o.name}
              k={o.id}
            >
              <div className="dmg-value">
                {(player.commanderDmg[
                  getPlayerIdx(player.commanderDmg, o.id)
                ] &&
                  player.commanderDmg[getPlayerIdx(player.commanderDmg, o.id)]
                    .dmg) ||
                  0}
              </div>
            </CounterController>
          </React.Fragment>
        );
      })}
    </Row>
  );
};
