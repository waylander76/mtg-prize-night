import { getPlayerIdx } from "./getPlayerIdx";

export function genericUpdPlayersStr(players, id, k, v) {
  const tempPlayers = players;
  tempPlayers[getPlayerIdx(tempPlayers, id)][k] = v;
  return tempPlayers;
}
