import React from "react";
import { map, reverse, sortBy } from "lodash";
import { Col, Row } from "components";

export const Leaderboard = ({ players }) => {
  return (
    <Col>
      {map(reverse(sortBy(players, ["points"])), (p) => {
        return (
          <Row key={p.id}>
            {p.name} - {p.points}
          </Row>
        );
      })}
    </Col>
  );
};
