import React from "react";
import { Col, Row } from "components";

export const Modal = ({ onYes, onNo, text }) => {
  return (
    <Col>
      {text}
      <Row>
        <button type="button" onClick={onYes}>
          YES
        </button>
        <button type="button" onClick={onNo}>
          NO
        </button>
      </Row>
    </Col>
  );
};
