import React from "react";

import "./PlayerCreationBoard.css";

export const PlayerCreationBoard = (props) => {
  const {
    player: { id, name },
    setPlayerName,
  } = props;

  return (
    <div key={id} className="player-details-container">
      <span>{id}</span>
      <input value={name} onChange={(e) => setPlayerName(id, e.target.value)} />
    </div>
  );
};
