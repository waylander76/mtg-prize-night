import { getPlayerIdx } from "./getPlayerIdx";

export function genericUpdPlayersNum(players, id, k, v) {
  const tempPlayers = players;
  tempPlayers[getPlayerIdx(tempPlayers, id)][k] += v;
  return tempPlayers;
}
