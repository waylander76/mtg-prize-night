import { random } from "lodash";
import { GameCreation } from "modules/GameCreation";
import { PlayersBoard } from "modules/PlayersBoard";
import React, { useCallback } from "react";
import { getPlayersNames } from "utils";
import { ConfirmationModal, Modal } from "components";
import { SessionManager } from "modules/SessionManager";

import "./Home.css";
import { HowToUse } from "modules/HowToUse";

export const Home = ({
  addRemoveNewPlayer,
  genericAddCounter,
  handleAddCommanderDmg,
  handleJoinSession,
  isFirstPlayerModalOn,
  isGameCreationIP,
  isGameIP,
  isResetIP,
  isSessionInUse,
  isSetNewGameIP,
  isUsePointsSystem,
  isUseTimer,
  players,
  playersRef,
  reset,
  selectedSession,
  setIsFirstPlayerModalOn,
  setIsGameCreationIP,
  setIsResetIP,
  setIsSetNewGameIP,
  setNewGame,
  setPlayerName,
  setSelectedSession,
  startNewGame,
  toggleIsUseTimer,
  toggleUsePointsSystem,
}) => {
  const handleResetClick = useCallback(() => {
    setIsResetIP(!isResetIP);
  }, [isResetIP, setIsResetIP]);

  const handleSetNewGameClick = useCallback(() => {
    setIsSetNewGameIP(!isSetNewGameIP);
  }, [isSetNewGameIP, setIsSetNewGameIP]);

  const setupNewGame = useCallback(() => {
    setIsGameCreationIP(!isGameCreationIP);
  }, [isGameCreationIP, setIsGameCreationIP]);

  const getRandomPlayer = useCallback(() => {
    const players = getPlayersNames(playersRef.current);
    return players[random(players.length - 1)];
  }, [playersRef]);

  if (isGameCreationIP) {
    return (
      <GameCreation
        players={players}
        addRemoveNewPlayer={addRemoveNewPlayer}
        startNewGame={startNewGame}
        setupNewGame={setupNewGame}
        setPlayerName={setPlayerName}
        isUsePointsSystem={isUsePointsSystem}
        toggleUsePointsSystem={toggleUsePointsSystem}
        isUseTimer={isUseTimer}
        toggleIsUseTimer={toggleIsUseTimer}
      />
    );
  }

  if (!isGameIP) {
    return (
      <>
        <button type="button" onClick={setupNewGame}>
          New game
        </button>
        <SessionManager
          handleJoinSession={handleJoinSession}
          isSessionInUse={isSessionInUse}
          setSelectedSession={setSelectedSession}
          selectedSession={selectedSession}
        />
        <HowToUse />
      </>
    );
  }

  if (isResetIP) {
    return (
      <Modal
        onYes={reset}
        onNo={handleResetClick}
        text="Reset the whole game and players?"
      />
    );
  }

  if (isSetNewGameIP) {
    return (
      <Modal
        onYes={setNewGame}
        onNo={handleSetNewGameClick}
        text="Start a new game with same players?"
      />
    );
  }

  return (
    <div className="container">
      {isFirstPlayerModalOn && (
        <ConfirmationModal
          onOk={() => setIsFirstPlayerModalOn(false)}
          text={`${getRandomPlayer()} goes first`}
        />
      )}
      <PlayersBoard
        players={players}
        genericAddCounter={genericAddCounter}
        handleResetClick={handleResetClick}
        handleSetNewGameClick={handleSetNewGameClick}
        isUsePointsSystem={isUsePointsSystem}
        addCommanderDmg={handleAddCommanderDmg}
        isUseTimer={isUseTimer}
      />
    </div>
  );
};
