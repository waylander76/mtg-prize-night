import React from "react";

import "./Col.css";

export const Col = (props) => {
  const { children } = props;

  return <div className="col">{children}</div>;
};
