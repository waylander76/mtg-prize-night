import React, { useCallback, useState } from "react";
import { map } from "lodash";
import { PlayerBoard } from "modules/PlayerBoard";
import { Leaderboard } from "modules/Leaderboard";
import { Col, Row } from "components";
import { Timer } from "components/Timer/Timer";

import "./PlayersBoard.css";

export const PlayersBoard = ({
  players,
  genericAddCounter,
  handleSetNewGameClick,
  handleResetClick,
  isUsePointsSystem,
  addCommanderDmg,
  isUseTimer,
}) => {
  const [isShowLeaderboard, setIsShowLeaderboard] = useState(false);

  const toggleShowLeaderboard = useCallback(() => {
    setIsShowLeaderboard(!isShowLeaderboard);
  }, [isShowLeaderboard]);

  return (
    <>
      {isUseTimer && <Timer />}
      <Col>
        {players &&
          map(players, (p) => {
            return (
              <React.Fragment key={p.id}>
                <PlayerBoard
                  player={p}
                  players={players}
                  onGenericAddCounter={genericAddCounter}
                  isUsePointsSystem={isUsePointsSystem}
                  addCommanderDmg={addCommanderDmg}
                />
              </React.Fragment>
            );
          })}
        {isShowLeaderboard && <Leaderboard players={players} />}
        <Row>
          <button type="button" onClick={handleSetNewGameClick}>
            NEW GAME
          </button>
          <button type="button" onClick={handleResetClick}>
            RESET ALL
          </button>
          {isUsePointsSystem && (
            <button type="button" onClick={toggleShowLeaderboard}>
              LEADERBOARD
            </button>
          )}
        </Row>
      </Col>
    </>
  );
};
