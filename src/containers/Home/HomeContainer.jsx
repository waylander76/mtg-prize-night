import { Home } from "modules/Home";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { map, isEqual } from "lodash";
import {
  genericUpdPlayersStr,
  genericUpdPlayersNum,
  getPlayerIdx,
} from "utils";
import { Col, Modal } from "components";
import useWebSocket, { ReadyState } from "react-use-websocket";
import { v4 as uuidv4 } from "uuid";

import "./HomeContainer.css";

const BE_URL = "http://localhost:3001/create-session";
const WS_URL = "ws://localhost:3001";

function isDocumentEvent(message) {
  const evt = JSON.parse(message.data);
  return evt.type === "contentchange";
}

export const HomeContainer = () => {
  const [players, setPlayers] = useState([]);
  const playersRef = useRef();
  playersRef.current = players;

  const [isFirstPlayerModalOn, setIsFirstPlayerModalOn] = useState(false);
  const [isGameCreationIP, setIsGameCreationIP] = useState(false);
  const [isGameIP, setIsGameIP] = useState(false);
  const [isResetIP, setIsResetIP] = useState(false);
  const [isSetNewGameIP, setIsSetNewGameIP] = useState(false);
  const [isUsePointsSystem, setIsUsePointsSystem] = useState(true);
  const [isUseTimer, setIsUseTimer] = useState(false);

  // used only for session controller
  const [isPublishingGameState, setIsPublishingGameState] = useState(undefined);
  const [isSessionInUse, setIsSessionInUse] = useState(false);
  const [selectedSession, setSelectedSession] = useState(undefined);
  const [isLeaveSessionIP, setIsLeaveSessionIP] = useState(false);

  const { sendJsonMessage, readyState } = useWebSocket(
    WS_URL,
    {
      onOpen: () => {
        console.log("WebSocket connection established.");
      },
      onMessage: () => {
        if (!isPublishingGameState) {
          updateGameState();
        } else {
          setIsPublishingGameState(false);
        }
      },
      share: true,
      filter: isDocumentEvent,
      retryOnError: true,
      shouldReconnect: () => true,
    },
    isSessionInUse
  );

  const updateGameState = useCallback(() => {
    let currData = undefined;
    fetch(`${BE_URL}/${selectedSession}`, {
      method: "GET",
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        currData = data.state;
        if (currData && currData.players) {
          if (!isEqual(currData.players, players)) {
            setPlayers(currData.players);
          }
          if (isGameIP !== currData.isGameIP) {
            setIsGameIP(currData.isGameIP);
          }
          if (isUsePointsSystem !== currData.isUsePointsSystem) {
            setIsUsePointsSystem(currData.isUsePointsSystem);
          }
          if (isUseTimer !== currData.isUseTimer) {
            setIsUseTimer(currData.isUseTimer);
          }
          if (isFirstPlayerModalOn !== currData.isFirstPlayerModalOn) {
            setIsFirstPlayerModalOn(currData.isFirstPlayerModalOn);
          }
        }
      })
      .catch((err) => console.log(err));
  }, [
    isFirstPlayerModalOn,
    isGameIP,
    isUsePointsSystem,
    isUseTimer,
    players,
    selectedSession,
  ]);

  useEffect(() => {
    if (readyState === ReadyState.OPEN) {
      sendJsonMessage({
        username: `${selectedSession}:${uuidv4()}`,
        type: "userevent",
      });
    }
  }, [sendJsonMessage, readyState, selectedSession]);

  const publishGameState = useCallback(
    (updatedObj) => {
      const newGameState = {
        players,
        isGameIP,
        isUsePointsSystem,
        isUseTimer,
        isFirstPlayerModalOn,
        ...updatedObj,
      };

      setIsPublishingGameState(true);
      fetch(`${BE_URL}/${selectedSession}`, {
        method: "PUT",
        body: JSON.stringify(newGameState),
        headers: {
          "Content-Type": "application/json",
        },
      }).catch((err) => console.log(err));

      //   broadcast msg on ws
      sendJsonMessage({
        type: "contentchange",
      });
    },
    [
      isFirstPlayerModalOn,
      isGameIP,
      isUsePointsSystem,
      isUseTimer,
      players,
      selectedSession,
      sendJsonMessage,
    ]
  );

  const handleJoinSession = useCallback(() => {
    setIsSessionInUse(true);
    setSelectedSession(selectedSession);
  }, [selectedSession]);

  const reset = useCallback(() => {
    setPlayers([]);
    setIsResetIP(false);
    setIsGameIP(false);
    setIsGameCreationIP(false);

    if (isSessionInUse) {
      publishGameState({
        players: [],
        isResetIP: false,
        isGameIP: false,
        isGameCreationIP: false,
      });
    }
  }, [isSessionInUse, publishGameState]);

  const startNewGame = useCallback(() => {
    setIsGameCreationIP(false);
    setIsGameIP(true);
    setIsFirstPlayerModalOn(true);

    if (isSessionInUse) {
      publishGameState({
        isFirstPlayerModalOn: true,
        isGameIP: true,
        isGameCreationIP: false,
      });
    }
  }, [isSessionInUse, publishGameState]);

  const toggleUsePointsSystem = useCallback(() => {
    setIsUsePointsSystem(!isUsePointsSystem);

    if (isSessionInUse) {
      publishGameState({ isUsePointsSystem: !isUsePointsSystem });
    }
  }, [isUsePointsSystem, isSessionInUse, publishGameState]);

  const toggleIsUseTimer = useCallback(() => {
    setIsUseTimer(!isUseTimer);

    if (isSessionInUse) {
      publishGameState({ isUseTimer: !isUseTimer });
    }
  }, [isUseTimer, isSessionInUse, publishGameState]);

  const handleSetIsFirstPlayerModalOn = useCallback(
    (val) => {
      setIsFirstPlayerModalOn(val);

      if (isSessionInUse) {
        publishGameState({ isFirstPlayerModalOn: val });
      }
    },
    [isSessionInUse, publishGameState]
  );

  const setNewGame = useCallback(() => {
    const tempPlayers = map([...playersRef.current], (p) => {
      return {
        id: p.id,
        name: p.name,
        points: p.points,
        life: 40,
        energy: 0,
        exp: 0,
        poison: 0,
        cmdrTax: 0,
        commanderDmg: [],
      };
    });
    setPlayers(tempPlayers);
    setIsSetNewGameIP(!isSetNewGameIP);
    setIsFirstPlayerModalOn(true);

    if (isSessionInUse) {
      publishGameState({
        players: tempPlayers,
        isSetNewGameIP: !isSetNewGameIP,
        isFirstPlayerModalOn: true,
      });
    }
  }, [isSetNewGameIP, playersRef, isSessionInUse, publishGameState]);

  const setPlayerName = useCallback(
    (id, v) => {
      const tempPlayers = genericUpdPlayersStr(
        [...playersRef.current],
        id,
        "name",
        v
      );
      setPlayers(tempPlayers);

      if (isSessionInUse) {
        publishGameState({ players: tempPlayers });
      }
    },
    [playersRef, isSessionInUse, publishGameState]
  );

  const genericAddCounter = useCallback(
    (id, v, k, shouldOmitUpdateAndReturn) => {
      const tempPlayers = genericUpdPlayersNum(
        [...playersRef.current],
        id,
        k,
        v
      );

      if (shouldOmitUpdateAndReturn) {
        return tempPlayers;
      }

      setPlayers(tempPlayers);

      if (isSessionInUse) {
        publishGameState({ players: tempPlayers });
      }
    },
    [publishGameState, isSessionInUse, playersRef]
  );

  const addRemoveNewPlayer = useCallback(
    (operation) => {
      let tempPlayers = [];
      switch (operation) {
        case "+":
          tempPlayers = [
            ...playersRef.current,
            {
              id: playersRef.current.length + 1,
              name: "",
              points: 0,
              life: 40,
              energy: 0,
              exp: 0,
              poison: 0,
              cmdrTax: 0,
              commanderDmg: [],
            },
          ];
          setPlayers(tempPlayers);

          if (isSessionInUse) {
            publishGameState({ players: tempPlayers });
          }
          return;
        case "-":
          tempPlayers = [...playersRef.current];
          tempPlayers.splice(-1);
          setPlayers(tempPlayers);

          if (isSessionInUse) {
            publishGameState({ players: tempPlayers });
          }
          return;
        default:
          return;
      }
    },
    [isSessionInUse, playersRef, publishGameState]
  );

  const handleAddCommanderDmg = useCallback(
    (v, o, p) => {
      let tempPlayers = [...playersRef.current];
      let tempCmdDmg = tempPlayers[getPlayerIdx(tempPlayers, p)].commanderDmg;
      if (tempCmdDmg.length === 0) {
        tempCmdDmg.push({
          id: o,
          dmg: v,
        });
      } else {
        const hasCmdDmgFromO =
          getPlayerIdx(tempCmdDmg, o) !== -1 ? true : false;
        if (hasCmdDmgFromO) {
          tempCmdDmg[getPlayerIdx(tempCmdDmg, o)].dmg += v;
        } else {
          tempCmdDmg.push({
            id: o,
            dmg: v,
          });
        }
      }

      const tempPlayersAfterLifeChanges = genericAddCounter(
        p,
        -v,
        "life",
        true
      );
      tempPlayersAfterLifeChanges[
        getPlayerIdx(tempPlayersAfterLifeChanges, p)
      ].commanderDmg = tempCmdDmg;
      setPlayers(tempPlayersAfterLifeChanges);

      if (isSessionInUse) {
        publishGameState({ players: tempPlayersAfterLifeChanges });
      }
    },
    [isSessionInUse, genericAddCounter, playersRef, publishGameState]
  );

  if (isLeaveSessionIP) {
    return (
      <Modal
        onYes={() => document.location.reload(true)}
        onNo={() => setIsLeaveSessionIP(false)}
        text="Leave the current session?"
      />
    );
  }

  return (
    <>
      {isSessionInUse && selectedSession && (
        <div
          className="current-session-container"
          onClick={() => setIsLeaveSessionIP(true)}
        >
          <Col>{selectedSession}</Col>
        </div>
      )}
      <Home
        addRemoveNewPlayer={addRemoveNewPlayer}
        genericAddCounter={genericAddCounter}
        handleAddCommanderDmg={handleAddCommanderDmg}
        handleJoinSession={handleJoinSession}
        isFirstPlayerModalOn={isFirstPlayerModalOn}
        isGameCreationIP={isGameCreationIP}
        isGameIP={isGameIP}
        isResetIP={isResetIP}
        isSetNewGameIP={isSetNewGameIP}
        isUsePointsSystem={isUsePointsSystem}
        isUseTimer={isUseTimer}
        isSessionInUse={isSessionInUse}
        players={players}
        playersRef={playersRef}
        reset={reset}
        selectedSession={selectedSession}
        setIsFirstPlayerModalOn={handleSetIsFirstPlayerModalOn}
        setIsGameCreationIP={setIsGameCreationIP}
        setIsGameIP={setIsGameIP}
        setIsResetIP={setIsResetIP}
        setIsSetNewGameIP={setIsSetNewGameIP}
        setIsUsePointsSystem={setIsUsePointsSystem}
        setIsUseTimer={setIsUseTimer}
        setNewGame={setNewGame}
        setPlayerName={setPlayerName}
        setPlayers={setPlayers}
        setSelectedSession={setSelectedSession}
        startNewGame={startNewGame}
        toggleIsUseTimer={toggleIsUseTimer}
        toggleUsePointsSystem={toggleUsePointsSystem}
      />
    </>
  );
};
