import React from "react";
import { Col, Row } from "components";

import "./ConfirmationModal.css";

export const ConfirmationModal = ({ onOk, text }) => {
  return (
    <div className="confirmation-modal">
      <Col>
        {text}
        <Row>
          <button type="button" onClick={onOk}>
            OK
          </button>
        </Row>
      </Col>
    </div>
  );
};
