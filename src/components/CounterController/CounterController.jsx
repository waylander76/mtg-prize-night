import React, { useCallback, useRef, useState } from "react";
import { Col, Row } from "components";

import "./CounterController.css";

export const CounterController = (props) => {
  const {
    onCounterClick,
    counterVal,
    desc,
    children,
    k,
    isAdditionalControllerEnabled,
    additionalCounterVal,
    isAdditional2ControllerEnabled,
    additional2CounterVal,
  } = props;
  const [cumulatedVal, setCumulatedVal] = useState(0);
  const [isCumulativeValVisible, setIsCumulativeValVisible] = useState(false);
  let timer = useRef();

  const saveInput = useCallback(() => {
    setIsCumulativeValVisible(false);
    setCumulatedVal(0);
  }, []);

  const handleCounterClick = useCallback(
    (v, k) => {
      onCounterClick(v, k);
      setCumulatedVal((cv) => cv + v);
      setIsCumulativeValVisible(true);

      clearTimeout(timer.current);
      timer.current = setTimeout(() => {
        saveInput();
      }, 3000);
    },
    [onCounterClick, saveInput]
  );

  return (
    <Col>
      <div className="counter-controller-container">
        <>{desc}</>
        <div
          className={`cumulated-value${!isCumulativeValVisible && "-hidden"}`}
        >
          {" "}
          {cumulatedVal}
        </div>
        <Row>
          {isAdditionalControllerEnabled && (
            <button
              type="button"
              onClick={() => handleCounterClick(-additionalCounterVal, k)}
            >
              - {additionalCounterVal}
            </button>
          )}
          {isAdditional2ControllerEnabled && (
            <button
              type="button"
              onClick={() => handleCounterClick(-additional2CounterVal, k)}
            >
              - {additional2CounterVal}
            </button>
          )}
          <button
            type="button"
            onClick={() => handleCounterClick(-counterVal, k)}
          >
            - {counterVal}
          </button>
          {children && children}
          <button
            type="button"
            onClick={() => handleCounterClick(counterVal, k)}
          >
            + {counterVal}
          </button>
          {isAdditional2ControllerEnabled && (
            <button
              type="button"
              onClick={() => handleCounterClick(additional2CounterVal, k)}
            >
              + {additional2CounterVal}
            </button>
          )}
          {isAdditionalControllerEnabled && (
            <button
              type="button"
              onClick={() => handleCounterClick(additionalCounterVal, k)}
            >
              + {additionalCounterVal}
            </button>
          )}
        </Row>
      </div>
    </Col>
  );
};
