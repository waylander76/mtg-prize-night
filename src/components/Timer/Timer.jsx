import React, { useCallback, useEffect, useState } from "react";

import "./Timer.css";

export const Timer = () => {
  const [isTimerActive, setIsTimerActive] = useState(false);
  const [time, setTime] = useState(0);

  useEffect(() => {
    let interval = null;

    if (isTimerActive) {
      interval = setInterval(() => {
        setTime((time) => time + 1);
      }, 1000);
    } else {
      clearInterval(interval);
      setTime(0);
    }
    return () => {
      clearInterval(interval);
    };
  }, [isTimerActive]);

  const handleToggleIsTimerActive = useCallback(() => {
    setIsTimerActive(!isTimerActive);
  }, [isTimerActive]);

  return (
    <div onClick={handleToggleIsTimerActive} className="timer">
      <span>
        {("0" + Math.floor(time / 60)).slice(-2)}:
        {("0" + Math.floor(time % 60)).slice(-2)}
      </span>
    </div>
  );
};
