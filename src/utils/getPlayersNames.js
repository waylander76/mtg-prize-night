import { map } from "lodash";

export function getPlayersNames(players) {
  return map(players, (p) => p.name);
}
