import React, { useCallback, useState } from "react";
import { Col, Row, CounterController } from "components";
import { CommanderDamageBoard } from "modules/CommanderDamageBoard";
import { getPlayersExcludingSelf } from "utils";

import "./PlayerBoard.css";

export const PlayerBoard = ({
  player,
  onGenericAddCounter,
  isUsePointsSystem,
  players,
  addCommanderDmg,
}) => {
  const [isEnergyCounterVisible, setIsEnergyCounterVisible] = useState(false);
  const [isExpCounterVisible, setIsExpCounterVisible] = useState(false);
  const [isPoisonCounterVisible, setIsPoisonCounterVisible] = useState(false);
  const [isCmdrTaxCounterVisible, setIsCmdrTaxCounterVisible] = useState(false);

  const handleGenericAddCounter = (v, k) => {
    onGenericAddCounter(player.id, v, k);
  };

  const toggleIsVisible = useCallback(
    (k) => {
      switch (k) {
        case "energy":
          setIsEnergyCounterVisible(!isEnergyCounterVisible);
          return;
        case "exp":
          setIsExpCounterVisible(!isExpCounterVisible);
          return;
        case "poison":
          setIsPoisonCounterVisible(!isPoisonCounterVisible);
          return;
        case "cmdrTax":
          setIsCmdrTaxCounterVisible(!isCmdrTaxCounterVisible);
          return;
        default:
          return;
      }
    },
    [
      isEnergyCounterVisible,
      isExpCounterVisible,
      isPoisonCounterVisible,
      isCmdrTaxCounterVisible,
    ]
  );

  return (
    <Col>
      <Row>
        {player.name}
        {isUsePointsSystem && <> {player.points}</>}
      </Row>
      <Row>
        {isEnergyCounterVisible && (
          <CounterController
            onCounterClick={handleGenericAddCounter}
            counterVal={1}
            desc={"energy"}
            k="energy"
          >
            <div className="counter-value">{player.energy}</div>
          </CounterController>
        )}
        {isExpCounterVisible && (
          <CounterController
            onCounterClick={handleGenericAddCounter}
            counterVal={1}
            desc={"experience"}
            k="exp"
          >
            <div className="counter-value">{player.exp}</div>
          </CounterController>
        )}

        <Col>
          <div onClick={() => toggleIsVisible("energy")}>en</div>
          <div onClick={() => toggleIsVisible("exp")}>exp</div>
        </Col>
        <CounterController
          onCounterClick={handleGenericAddCounter}
          counterVal={1}
          desc={"LIFE"}
          k="life"
          isAdditionalControllerEnabled
          additionalCounterVal={5}
          isAdditional2ControllerEnabled
          additional2CounterVal={3}
        >
          <div className="counter-value">{player.life}</div>
        </CounterController>
        <Col>
          <div onClick={() => toggleIsVisible("poison")}>poi</div>
          <div onClick={() => toggleIsVisible("cmdrTax")}>cmd</div>
        </Col>

        {isPoisonCounterVisible && (
          <CounterController
            onCounterClick={handleGenericAddCounter}
            counterVal={1}
            desc={"poison"}
            k="poison"
          >
            <div className="counter-value">{player.poison}</div>
          </CounterController>
        )}
        {isCmdrTaxCounterVisible && (
          <CounterController
            onCounterClick={handleGenericAddCounter}
            counterVal={1}
            desc={"commander tax"}
            k="cmdrTax"
          >
            <div className="counter-value">{player.cmdrTax}</div>
          </CounterController>
        )}
      </Row>

      <CommanderDamageBoard
        opponents={getPlayersExcludingSelf(players, player.id)}
        addCommanderDmg={addCommanderDmg}
        player={player}
      />

      {isUsePointsSystem && (
        <>
          <Row>
            <CounterController
              onCounterClick={handleGenericAddCounter}
              counterVal={1}
              desc={"dmg / poison counter / discard (1)"}
              k="points"
              isAdditionalControllerEnabled
              additionalCounterVal={5}
            />
            <CounterController
              onCounterClick={handleGenericAddCounter}
              counterVal={0.5}
              desc={"life gain (0,5)"}
              k="points"
              isAdditionalControllerEnabled
              additionalCounterVal={2}
            />
          </Row>
          <Row>
            <CounterController
              onCounterClick={handleGenericAddCounter}
              counterVal={2}
              desc={"enchantment / non c artifact destroyed"}
              k="points"
            />
            <CounterController
              onCounterClick={handleGenericAddCounter}
              counterVal={5}
              desc={"player kill"}
              k="points"
            />
          </Row>
        </>
      )}
    </Col>
  );
};
