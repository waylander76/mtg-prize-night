import { filter } from "lodash";

export function getPlayersExcludingSelf(players, id) {
  return filter(players, (p) => Number(p.id) !== Number(id));
}
