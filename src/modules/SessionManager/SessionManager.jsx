import React, { useState, useEffect, useCallback } from "react";
import { map } from "lodash";
import { Col, Modal, Row } from "components";

import "./SessionManager.css";

const BE_URL = "http://localhost:3001/create-session";

export const SessionManager = ({
  handleJoinSession,
  selectedSession,
  setSelectedSession,
  isSessionInUse,
}) => {
  const [sessions, setSessions] = useState([]);
  const [isCreateSessionIP, setIsCreateSessionIP] = useState(false);
  const [isJoinSessionIP, setIsJoinSessionIP] = useState(false);

  const fetchSessions = useCallback(() => {
    fetch(BE_URL)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setSessions(map(data, (s) => s.sessionId));
      })
      .catch((err) => console.log(err));
  }, []);

  const createSession = useCallback(() => {
    fetch(BE_URL, { method: "POST" })
      .then(() => {
        fetchSessions();
        setIsCreateSessionIP(false);
      })
      .catch((err) => console.log(err));
  }, [fetchSessions]);

  const toggleJoinSessionIP = useCallback(
    (shouldUpdateSessionId = false, sessionId) => {
      if (shouldUpdateSessionId) {
        setSelectedSession(sessionId);
      }
      setIsJoinSessionIP(!isJoinSessionIP);
    },
    [isJoinSessionIP, setSelectedSession]
  );

  const joinSession = useCallback(() => {
    handleJoinSession();
    toggleJoinSessionIP();
  }, [handleJoinSession, toggleJoinSessionIP]);

  const toggleCreateSessionIP = useCallback(() => {
    setIsCreateSessionIP(!isCreateSessionIP);
  }, [isCreateSessionIP]);

  useEffect(() => {
    fetchSessions();
  }, [fetchSessions]);

  if (isCreateSessionIP) {
    return (
      <Modal
        onYes={createSession}
        onNo={toggleCreateSessionIP}
        text="Create a new session?"
      />
    );
  }

  if (isJoinSessionIP) {
    return (
      <Modal
        onYes={joinSession}
        onNo={toggleJoinSessionIP}
        text={`Join the session: ${selectedSession}?`}
      />
    );
  }

  if (isSessionInUse) {
    return <div>Create a new game or wait for someone to enter players</div>;
  }

  return (
    <Col>
      <div className="session-manager-join-create">
        Join a session or create a new one
        <Row>
          <span>current sessions: {sessions.length}</span>
          <button type="button" onClick={fetchSessions}>
            refresh
          </button>
        </Row>
        <button type="button" onClick={toggleCreateSessionIP}>
          create a new session
        </button>
        {sessions.length > 0 &&
          sessions.map((s) => (
            <Row key={s}>
              <div
                className="session"
                onClick={() => toggleJoinSessionIP(true, s)}
              >
                {s}
              </div>
            </Row>
          ))}
      </div>
    </Col>
  );
};
