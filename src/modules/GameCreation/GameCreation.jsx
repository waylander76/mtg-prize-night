import React from "react";
import { map } from "lodash";
import { PlayerCreationBoard } from "modules/PlayerCreationBoard";
import { Col, Row } from "components";

import "./GameCreation.css";

export const GameCreation = (props) => {
  const {
    players,
    addRemoveNewPlayer,
    startNewGame,
    setupNewGame,
    setPlayerName,
    isUsePointsSystem,
    toggleUsePointsSystem,
    isUseTimer,
    toggleIsUseTimer,
  } = props;

  return (
    <Col>
      <>Number of players: {players.length}</>
      <Row>
        <button type="button" onClick={() => addRemoveNewPlayer("+")}>
          +
        </button>
        <button type="button" onClick={() => addRemoveNewPlayer("-")}>
          -
        </button>
      </Row>
      <div className="player-creation-board-container">
        {map(players, (p) => {
          return (
            <React.Fragment key={p.id}>
              <PlayerCreationBoard player={p} setPlayerName={setPlayerName} />
            </React.Fragment>
          );
        })}
      </div>
      <label>
        Use points system
        <input
          type="checkbox"
          checked={isUsePointsSystem}
          onChange={toggleUsePointsSystem}
        />
      </label>
      <label>
        Use timer
        <input
          type="checkbox"
          checked={isUseTimer}
          onChange={toggleIsUseTimer}
        />
      </label>
      <Row>
        <button type="button" onClick={startNewGame}>
          FINISH
        </button>
        <button type="button" onClick={setupNewGame}>
          CANCEL
        </button>
      </Row>
    </Col>
  );
};
