import { Col, Row } from "components";
import { useCallback, useState } from "react";

export const CollapsibleBox = ({ title, children, subTitle }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleIsOpen = useCallback(() => {
    setIsOpen(!isOpen);
  }, [isOpen]);

  return (
    <Col>
      <div onClick={toggleIsOpen}>
        <Col>
          <Row>
            <h4>{title} </h4>
            {isOpen ? <>&uarr;</> : <>&darr;</>}
          </Row>
          {subTitle && <h5>{subTitle}</h5>}
        </Col>
      </div>
      {isOpen && children}
    </Col>
  );
};
