import { Col } from "components";
import { CollapsibleBox } from "components/CollapsibleBox";

export const HowToUse = () => {
  return (
    <Col>
      <h3>HOW TO USE</h3>
      <CollapsibleBox
        title={"WITHOUT SESSION"}
        subTitle="(control everything on one phone)"
      >
        <span>click on "New game", set players and click "FINISH"</span>
      </CollapsibleBox>
      <CollapsibleBox
        title={"WITH SESSION"}
        subTitle="(each player has access to the players board)"
      >
        <span>
          join a session by clicking on it, once you've joined the session, the
          ID of the session will appear on the top right corner
        </span>
        <span>
          if no one set the players already, click on "New game", set players
          and click "FINISH"
        </span>
        <span>
          if someone already set the players, just join the session and the game
          state will load
        </span>
        <span>
          you can leave the session any time by click on it in the top right
          corner
        </span>
      </CollapsibleBox>
      <CollapsibleBox title={"GENERAL"}>
        <span>
          if using timer, clicking on it in the top left corner stops it, resets
          it and starts it
        </span>
        <span>
          adding commander damage from one player instantly reduces the health
          of the player receiving the damage
        </span>
        <span>
          you can expand various counter trackers by clicking on the
          abbreviation in the columns left and right from a life counter
        </span>
        <span>
          in a running game, when you click on a "NEW GAME": you will start a
          new game with the same players, resetting their life, counters and
          damage, but not the points they've accumulated in case you're playing
          with points system
        </span>
        <span>
          in a running game, when you click on a "RESET ALL": you will start a
          new game and need to create new players, all points will be lost
        </span>
        <span>
          when using points system, if a box has more than one counter option,
          you will find the singular accumulation value in the parenthesis next
          to it
        </span>
      </CollapsibleBox>
    </Col>
  );
};
